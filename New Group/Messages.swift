//
//  Messages.swift
//  Raabta
//
//  Created by rashid  mahmood on 7/22/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class Messages: NSObject {
    
    
    var toId: String?
    var fromId: String?
    var text: String?
    var time: NSNumber?
    var chatPartnerId: String?
    
    var imageUrl: String?
    var imageHeight: NSNumber?
    var imageWidth: NSNumber?
    
    
    
    
    var fromEmail: String?
    var toEmail: String?
    var chatPartnerEmail: String?
    
    
    func  chatPartnerID() -> String? {

        if toId != Auth.auth().currentUser?.uid
        {
            return toId
        }
        else
        {
            return fromId
        }

    }
    
}
