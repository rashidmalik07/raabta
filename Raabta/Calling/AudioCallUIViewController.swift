//
//  AudioCallUIViewController.swift
//  Raabta
//
//  Created by rashid  mahmood on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
//import Sinch

class AudioCallUIViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
//    func callingEmail(_sender: CallTableViewCell, callingId: String) {
//        
//    }
  
    @IBOutlet weak var callTableView: UITableView!
    
            var groupEmails = [GroupChat]()
            var selectedEmails = [GroupChat]()
           // var selectArr = [GroupChat]()
            var databaseHandle : DatabaseHandle?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
//            let callClient = RCallClient.sharedCallClient
//            callClient.delegate = self
//            RCallClient.performSettings()
//            RCallClient.startSharedClient()
//
            callTableView.backgroundColor = UIColor.clear
            callTableView.delegate  = self
            callTableView.dataSource = self
            
           // self.tabBarController?.tabBar.isHidden = true
            
            self.navigationItem.title = "Call"
            let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.purple]
            navigationController?.navigationBar.titleTextAttributes = textAttributes
            
            let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "GroupBackground")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
            //self.callTableView.isEditing = true
           // self.callTableView.allowsMultipleSelectionDuringEditing = true
            
            fetchUser()
            
    //        self.tabBarController?.tabBar.isHidden = false
        }
        
          func fetchUser () {
            let ref = Database.database()
            databaseHandle = ref.reference().child("User").observe(.childAdded, with: { (snapshot) in
                
    //             print(snapshot)
                if let dictionary = snapshot.value as? [String : AnyObject] {
                   let groupEmails = GroupChat()
                        groupEmails.id = snapshot.key
                        groupEmails.email = dictionary["Email"] as? String
                    
                        self.groupEmails.append(groupEmails)
                        self.callTableView.reloadData()
                }
         
            }, withCancel: nil)
            
        }
        
        
           func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return groupEmails.count
        }
        
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let currentObj = groupEmails[indexPath.row]
            let cell = callTableView.dequeueReusableCell(withIdentifier: "CallTableViewCell", for: indexPath) as? CallTableViewCell
                cell?.chatOBject = currentObj
                cell?.textLabel?.text = currentObj.email
                cell?.layer.backgroundColor = UIColor.clear.cgColor
                cell?.contentView.backgroundColor = UIColor.clear
                cell?.textLabel?.textColor = UIColor.white
                cell?.delegate = self 
                return cell ?? UITableViewCell()
        }
        
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
               //self.selectDeselectCell(tableView: callTableView, indexPath: indexPath)
                print("deselect")
                
                }

        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           // var selectArr = [String]()
            print(groupEmails[indexPath.row].email as Any)
        }

}

extension AudioCallUIViewController: CallTableViewCellProtocol  {
    func callSegueFromCell(myData dataobject: String) {
        let storyboard = UIStoryboard(name: "Call", bundle: nil)
            let callVC = storyboard.instantiateViewController(withIdentifier: "CallScreenViewController") as! CallScreenViewController
            callVC.username = "adeel@gmail.com" 
            self.navigationController?.pushViewController(callVC, animated: true)
    }

}



