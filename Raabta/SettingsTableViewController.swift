//
//  SettingsTableViewController.swift
//  Raabta
//
//  Created by rashid  mahmood on 5/11/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD
import FirebaseDatabase

class SettingsTableViewController: UITableViewController {

  
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // callTableView.backgroundColor = UIColor.clear
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "GroupBackground")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    @IBAction func goBackToOneButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "unwindSegueToSignUpViewController", sender: self)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "abc", for: indexPath) as! SettingsTableViewCell
        tableView.backgroundColor = UIColor.clear
        
        switch indexPath.row {
        case 0:
            cell.settingName?.textColor = .white
            cell.settingName?.text = "Profile Image"
            
        case 1:
            cell.settingName?.text = "Help"
        case 2:
            cell.settingName?.text = "Sign Out"
        default:
            cell.settingName?.text = ""
        }

        return cell
    
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("Profile")
            performSegue(withIdentifier: "showProfileSegue", sender: self)
            
        case 1:
            print("Help")
            performSegue(withIdentifier: "showHelpSegue", sender: self)
        case 2:
            print("Logout")
            logoutAlert()
            //performSegue(withIdentifier: "showLogoutSegue", sender: self)
        default:
            print("No row selected")
        }
    }
    
    
    @objc func handleSelectProfileImageView() {
        print("123")
    }
    
    
    

    func logoutAlert() -> Void {
        let signOutAction = UIAlertAction( title: "Sign Out", style: .destructive) { (action) in
//            do {
//                SVProgressHUD.show(withStatus: "Loading...")
//                try Auth.auth().signOut()
//                self.goBackToOneButtonTapped(self)
//                SVProgressHUD.dismiss()
//            }
//            catch let err {
//                print("failed to Signout with error", err)
//            }
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            UserDefaults.standard.set(false, forKey: "login")
            self.present(vc, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        let alert = UIAlertController(title: "Sign Out!", message: "Are you sure, you want to sign out", preferredStyle: .actionSheet)// yeh parameter decidkrta ha ok

        alert.addAction(signOutAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
        
        
    }

}
