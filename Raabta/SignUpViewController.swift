//
//  SignUpViewController.swift
//  Raabta
//
//  Created by rashid  mahmood on 5/1/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD
import FirebaseDatabase


class SignUpViewController: UIViewController {

    @IBAction func unwindToSignUpViewController(segue:UIStoryboardSegue) { }
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    

    @IBOutlet weak var errorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func createAccount(_ sender: AnyObject) {
        
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        Auth.auth().createUser(withEmail: userName.text!, password: password.text!, completion:  {
            
            user, error in
            
            SVProgressHUD.dismiss()
            self.dismiss(animated: true, completion: nil)
            
            if error != nil{
                self.errorLabel.text = error?.localizedDescription
            }
            else {
                self.errorLabel.text = ""
            }
            
///             user authenticated successful ...
            
            
            // sending data to database
            
            guard let uid = Auth.auth().currentUser?.uid else{
                return
            }
            
            let ref = Database.database().reference().child("User")
            
            ref.child(uid).setValue(["Pass": self.password.text , "Email": self.userName.text])

            // sending data to database sucessfull
            
        })
        
    }
    
    @IBAction func loginAccount(_ sender: AnyObject) {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        self.login()

    }
    
   
    
    func login(){
        Auth.auth().signIn(withEmail: userName.text!, password: password.text!, completion: {
            
            user, error in
            
            SVProgressHUD.dismiss()
            
            if error != nil{
                self.errorLabel.text = error?.localizedDescription
            }
            else {
                self.errorLabel.text = ""
                self.performSegue(withIdentifier: "showTabBar", sender: self)
                //session 
                UserDefaults.standard.set(true, forKey: "login")
                print("session saved")
            }
           
            Auth.auth().signInAnonymously(completion: nil)
            //self.dismiss(animated: true, completion: nil)
            
            
        })
        
    }
    
    
    
    
    
    
    
    

}
