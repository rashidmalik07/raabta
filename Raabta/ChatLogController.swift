//
//  ChatLogController.swift
//  Raabta
//
//  Created by rashid  mahmood on 7/19/19.
//  Copyright © 2019 Apple. All rights reserved.
//


import UIKit
import Foundation
import FirebaseStorage
import FirebaseDatabase
import Firebase
import SDWebImage
import AVFoundation
import MobileCoreServices


class ChatLogController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var chatTextField: UITextField!
    
    var email = String()
    var toUserId = String()

    var user = [User]()
    var messages = [Messages]()
    
    let cellId = "cellId"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.alwaysBounceVertical = true
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        collectionView.register(chatMessageCell.self, forCellWithReuseIdentifier: cellId)
        
        //print("Email====",email)

        navigationItem.title = email

//        if email != ""
//        {
//            navigationItem.title = email
//
//        }
//        else if toUserId != ""
//        {
//            navigationItem.title = toUserId
//
//        }
        
        observeChatMessages()
        setupKeyboardObserver()
        
    }
    
    func observeChatMessages() {

        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let userChatMessagesRef = Database.database().reference().child("user-messages").child(uid).child(toUserId)
        userChatMessagesRef.observe(.childAdded, with: { (snapshot) in
            
            let messageId = snapshot.key
            let messageRef = Database.database().reference().child("messages").child(messageId)
            messageRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dictonary = snapshot.value as? [String: AnyObject]{
                    
                    let message = Messages()
                    
                    message.fromId = dictonary["fromId"] as? String
                    message.toId = dictonary["toId"] as? String
                    message.text = dictonary["text"] as? String
                    message.time = dictonary["time"] as? NSNumber
                    message.imageUrl = dictonary["imageUrl"] as? String
                    message.imageWidth = dictonary["imageWidth"] as? NSNumber
                    message.imageHeight = dictonary["imageHeight"] as? NSNumber
                    
                    
                    
                
                    if message.toId != Auth.auth().currentUser?.uid {
                        message.chatPartnerId = message.toId
                    }
                    else
                    {
                        message.chatPartnerId = message.fromId
                    }
                    
                    if message.chatPartnerId == self.toUserId
                    {
                        self.messages.append(message)
                        self.collectionView.reloadData()
                        
                        let indexPath = NSIndexPath(item: self.messages.count - 1, section: 0)
                        self.collectionView.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                    }
                    
                }
                
            }, withCancel: nil)
        }, withCancel: nil)
    }
    
    func setupKeyboardObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    @objc func handleKeyboardDidShow()
    {
        if messages.count > 0
        {
            let indexPath = NSIndexPath(item: messages.count - 1, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: .top, animated: true)
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! chatMessageCell
//        cell.backgroundColor = UIColor.purple
        
        
        let message = messages[indexPath.row]
        cell.textView.text = message.text
        
        if let text = message.text
        {
            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: text).width + 25
            cell.textView.isHidden = false
        }
        else if message.imageUrl != nil
        {
            // fall in here code did not found "text" in firebase
            cell.bubbleWidthAnchor?.constant = 200
            cell.textView.isHidden = true
        }
        
        setupCell(cell: cell, message: message)
        return cell
    
    }
    
    func setupCell(cell: chatMessageCell , message: Messages)
    {
        
        
        // creating dynamic cells 
        if message.fromId == Auth.auth().currentUser?.uid
        {
            // Outgoing Purpal Message
            cell.bubbleView.backgroundColor = chatMessageCell.purpalColor
            cell.profileImageView.isHidden = true
            
            cell.bubbleViewRightAnchor?.isActive = true
            cell.bubbleViewLeftAnchor?.isActive = false
            
        }
        else
        {
            // Incoming Gray Message
            cell.bubbleView.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
            cell.textView.textColor = UIColor.black
            
            cell.profileImageView.isHidden = false
            cell.bubbleViewRightAnchor?.isActive = false
            cell.bubbleViewLeftAnchor?.isActive = true
        }
        
        
        
        
        // image loading using SDWebImage Cocapod
        if let messageImageUrl = message.imageUrl
        {
            cell.messageImageView.sd_setImage(with: URL(string: messageImageUrl))
            cell.messageImageView.isHidden = false
            cell.bubbleView.backgroundColor = UIColor.clear
            
            
            cell.messageImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomIn)))
            
        }
        else
        {
            cell.messageImageView.isHidden = true
        }
        
    }
    
    
    var startingFrame: CGRect?
    var blackBackgroundView: UIView?
    var startingImageView: UIImageView?
    
    @objc func handleZoomIn(tapGesture: UITapGestureRecognizer)
    {
//        print("hande zoom")
        startingImageView = tapGesture.view as? UIImageView
        startingImageView?.isHidden = true
        startingFrame = startingImageView?.superview?.convert(startingImageView!.frame, to: nil)
//        print(startingFrame!)
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.backgroundColor = UIColor.red
        zoomingImageView.image = startingImageView?.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        if let keyWindow = UIApplication.shared.keyWindow
        {
            
            blackBackgroundView = UIView(frame: keyWindow.frame)
            blackBackgroundView?.backgroundColor = UIColor.black
            blackBackgroundView?.alpha = 0
            keyWindow.addSubview(blackBackgroundView!)
            keyWindow.addSubview(zoomingImageView)

            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                
                self.blackBackgroundView!.alpha = 1
                //  maths here
                //  h2 / w2 = h1 / w1
                //  h2 = h1 / w1 * w2
                
                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindow.frame.width
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.center = keyWindow.center
                
            }, completion: nil)
        }
        
    }
    
    @objc func handleZoomOut(tapGesture: UITapGestureRecognizer)
    {
//        print("Zoom Out")
        if let zoomOutImageView = tapGesture.view
        {
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                
                zoomOutImageView.frame = self.startingFrame!
                self.blackBackgroundView?.alpha = 0
                
            }) { (completed: Bool) in
                
                zoomOutImageView.removeFromSuperview()
                self.startingImageView?.isHidden = false
                
            }
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        if let text = message.text
        {
            height = estimateFrameForText(text: text).height + 20
        }
        else if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue
        {
            
            //  maths here
            //  h2 / w2 = h1 / w1
            //  h2 = h1 / w1 * w2
            height = CGFloat(imageHeight / imageWidth * 200)
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    private func estimateFrameForText(text: String) -> CGRect{

        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)

        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], context: nil)
    }

    
    
    
    
    
    @IBOutlet weak var sndimageview: UIImageView!
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        
        sndimageview.isUserInteractionEnabled = true
        sndimageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUploadTapGesture)))
        
    }
    
    //  handle picture sending tap gesture
    @objc func handleUploadTapGesture(){
//        print("we tapped")
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [(kUTTypeImage as String), (kUTTypeMovie as String)]
        
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        
        
        if let localMovieURL = Bundle.main.url(forResource: "TestMovie", withExtension: "mov")
        {
            let randomID = UUID.init().uuidString
            let uploadRef = Storage.storage().reference(withPath: "/movies/\(randomID).mov")

            let uploadMetadata = StorageMetadata()
            uploadMetadata.contentType = "video/quicktime"

            let taskReference = uploadRef.putFile(from: localMovieURL, metadata: uploadMetadata) { (metadata, error) in

                if let error = error
                {
                    print("UPLOAD FAILED: \(error)")
                    return
                }

                print("UPLAOD COMPLETED: ... \(String(describing: metadata))")
            }
            
            
            taskReference.observe(.progress){[weak self] (snapshot) in
                guard let pcThere = snapshot.progress?.fractionCompleted else
                {
                    return
                }
                
                print("You are \(pcThere) completed")
//                self.progressView.progress = Float(pcThere)
            }

        }
        
        
        
        
        
        
        
        
        
        
        
//        if let localMovieURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
//        {
//            let randomID = UUID.init().uuidString
//            let uploadRef = Storage.storage().reference(withPath: "/movies/\(randomID).mov")
//
//            let uploadMetadata = StorageMetadata()
//            uploadMetadata.contentType = "video/quicktime"
//
//            let taskReference = uploadRef.putFile(from: localMovieURL, metadata: uploadMetadata) { (metadata, error) in
//
//                if let error = error
//                {
//                    print("UPLOAD FAILED: \(error)")
//                    return
//                }
//
//                print("UPLAOD COMPLETED: ... \(String(describing: metadata))")
//            }
//        }
      

        
        
        
        
        
        
        
        
//        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL
//        {
//            print("here is the Video URL: ",videoUrl)
//
//            let filename = "someFilename.mov"
//            Storage.storage().reference().child(filename).putFile(from: videoUrl, metadata: nil) { (metadata, error) in
//
//
//                if error != nil
//                {
//                    print("FAILED UPLOAD: ", error!)
//                    return
//                }
//
//                Storage.storage().reference().downloadURL { (url, error) in
//
//                    if error != nil
//                    {
//                        print("URL ERROR: ",error!)
//                        return
//                    }
//
//                    let StorageURL = url?.absoluteString
//
//                    print("Storage URL: ",StorageURL!)
//
//                }
//
//            }
//
//        }
        
        
        
        
        
        
        
        
        
        
        
        var seletedImageFromPicker: UIImage?

        if let editImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{

            seletedImageFromPicker = editImage

        }
        else if let orignalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            seletedImageFromPicker = orignalImage

        }
        
        if let seletedImage = seletedImageFromPicker{

            uploadToFirebaseStorageUsingImage(image: seletedImage)
        }

        dismiss(animated: true, completion: nil)
     
    }
    
    private func uploadToFirebaseStorageUsingImage(image: UIImage) {
        
        let imageName = NSUUID().uuidString
        let ref = Storage.storage().reference().child("message_images").child(imageName)

        
        if let uploadData = image.jpegData(compressionQuality: 0.2)
        {
            ref.putData(uploadData, metadata: nil) { (metadata, error) in
                
                if error != nil
                {
                    print("Failed to Upload",error!)
                    return
                }
                
//                let URL = URL.absoluteString
                ref.downloadURL(completion: {(url, error) in
                    if error != nil {
                     print(error!.localizedDescription)
                       return
                    }
                    let ImageURL = url?.absoluteString
                 //   let values: Dictionary<String, Any> = ["download_url": downloadURL]
//                    print("Image URL : " ,ImageURL!)
                    
                    self.sendImageUrl(imageUrl: ImageURL!, image: image)
                })
                
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sendButton(_ sender: UIButton) {
        // firebase main data save krta ha text messages
        
        let properties = ["text": self.chatTextField.text!] as [String : Any]
        SendMessagesWithProperties(properties: properties as [String : AnyObject])
        
    }
    
    private func sendImageUrl(imageUrl: String , image:UIImage){
     // fire base main image and us ka url save krta ha almost to sendbutton func but diff
        
        let properties: [String: Any ] = ["imageUrl": imageUrl, "imageWidth": image.size.width , "imageHeight": image.size.height]
     
        SendMessagesWithProperties(properties: properties as [String : AnyObject])
       
    }
    
    
    
    private func SendMessagesWithProperties(properties: [String: AnyObject])
    {
        

        // fire base main image and us ka url save krta ha almost to sendbutton func but diff
           

           Database.database().reference().child("User").observe(.childAdded, with: { (snapshot) in
                      if let dictionary = snapshot.value as? [String: AnyObject]{
                          let user = User()
                          user.toId = snapshot.key
                          
                          user.Email = dictionary["Email"] as? String
                          

                          if user.Email == self.email{
                              
                          //    let fromEmail = Auth.auth().currentUser?.email
                              let fromId = Auth.auth().currentUser?.uid
                              
                              let ref = Database.database().reference().child("messages")
                              let childRef = ref.childByAutoId()
                              
                              let time: NSNumber = NSNumber(value: Int(NSDate().timeIntervalSince1970))
                              
                            var values: [String:AnyObject] = ["toId": user.toId! , "fromId": fromId! , "time": time] as [String : AnyObject]
                            
                            properties.forEach({values[$0] = $1})
                              
                              childRef.updateChildValues(values) { (error, ref) in
                                  
                                  if error != nil{
                                      print(error!)
                                      return
                                  }
                              
                                  let messageId = childRef.key
                                  let valuess = ["txt": self.chatTextField.text]
                                  
                                  let userMessagesRef = Database.database().reference().child("user-messages").child(fromId!).child(user.toId!)
                                  userMessagesRef.child(messageId!).setValue(valuess)
                                  
                                  let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(user.toId!).child(fromId!)
                                  recipientUserMessagesRef.child(messageId!).setValue(valuess)
                              }
                              
                                  self.chatTextField.text = nil
                              
                              
                              /*  self.chatTextField.text = nil
                               
                               is sayy chat text field to clear ho jatii ha but firebase main text save ni hota
                               
                               */
                              

                          }
                          else if user.toId == self.toUserId {
                              
                              let fromId = Auth.auth().currentUser?.uid
                              
                              let ref = Database.database().reference().child("messages")
                              let childRef = ref.childByAutoId()
                              
                              let time: NSNumber = NSNumber(value: Int(NSDate().timeIntervalSince1970))
                              
                              var values: [String:AnyObject] = ["toId": user.toId! , "fromId": fromId! , "time": time] as [String : AnyObject]
                              
                              properties.forEach({values[$0] = $1})
                              
                              
                              
                              childRef.updateChildValues(values) { (error, ref) in
                                  
                                  if error != nil{
                                      print(error!)
                                      return
                                  }
                                  
                                  
                                  let messageId = childRef.key
                                  let valuess = ["txt": self.chatTextField.text]
                                  
                                  let userMessagesRef = Database.database().reference().child("user-messages").child(fromId!).child(user.toId!)
                                  userMessagesRef.child(messageId!).setValue(valuess)
                                  
                                  let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(user.toId!).child(fromId!)
                                  recipientUserMessagesRef.child(messageId!).setValue(valuess)
                              }
                                  self.chatTextField.text = nil

                            /*  self.chatTextField.text = nil
                               
                               is sayy chat text field to clear ho jatii ha but firebase main text save ni hota
                               
                              */

                          }
                          else
                          {
                              return
                          }
                          
                          self.user.append(user)

                      }
                  }, withCancel: nil)
        
        
    }
    

}
