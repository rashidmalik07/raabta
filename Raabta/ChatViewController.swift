    

import UIKit
import Firebase
import FirebaseDatabase

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellID = "cellID"
    var messages = [Messages]()
     var user = [User]()
    var messagesDictonary = [String: Messages]()

    @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   observeMessages()
        
        messages.removeAll()
        messagesDictonary.removeAll()
        tableView.reloadData()
        
        observeUserMessages()
        
    }
    
    
    func observeUserMessages(){
        
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        
        ref.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key
            Database.database().reference().child("user-messages").child(uid).child(userId).observe(.childAdded, with: { (snapshot) in
                
                let messageId = snapshot.key
                
                let messagesRef = Database.database().reference().child("messages").child(messageId)
                
                messagesRef.observeSingleEvent(of: .value, with: { (snapshott) in
                    
                    if let dictonary = snapshott.value as? [String: AnyObject] {
                        
                        let message = Messages()
                        
                        
                        // Data is saving to dictonary fetched from firebase
                        
                        message.toId = dictonary["toId"] as? String
                        message.fromId = dictonary["fromId"] as? String
                        
                        message.toEmail = dictonary["toEmail"] as? String
                        message.text = dictonary["text"] as? String
                        message.time = dictonary["time"] as? NSNumber
                        message.fromEmail = dictonary["fromEmail"] as? String
                        //                    self.messages.append(message)
                        
                        //      Partners Chat Code Only
                        if message.toId != Auth.auth().currentUser?.uid {
                            message.chatPartnerId = message.toId
                        }
                        else
                        {
                            message.chatPartnerId = message.fromId
                        }
                        // sortin code with time (new message arrival)
                        if let chatPartnerID = message.chatPartnerId{
                            
                            self.messagesDictonary[chatPartnerID] = message
                            
                        }
                        
                        self.attemptReloadTable()
                        
                      
                    }

                }, withCancel: nil)
                
            }, withCancel: nil)
            
        }, withCancel: nil)
        
    }
    
    private func attemptReloadTable() {
        
        self.timer?.invalidate()
        //print("disabled timer")
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
        //print("enabled timer")
        
    }
    
    var timer: Timer?
    @objc func handleReloadTable() {
        
        self.messages = Array(self.messagesDictonary.values)
        self.messages.sort(by: { (m1, m2) -> Bool in
            
            return m1.time!.intValue > m2.time!.intValue
            
        })
        
        self.tableView.reloadData()
        print("table reloaded")
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messages.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cellID")
        let message = messages[indexPath.row]

        if let toId = message.chatPartnerId{
            let ref = Database.database().reference().child("User").child(toId)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if let dictonary = snapshot.value as? [String: AnyObject]
                {
                    cell.textLabel?.text = dictonary["Email"] as? String
                }
            }, withCancel: nil)
            
            cell.detailTextLabel?.text = message.text
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let message = messages[indexPath.row]
        
        if message.toId != Auth.auth().currentUser?.uid {
            message.chatPartnerId = message.toId
        }
        else
        {
            message.chatPartnerId = message.fromId
        }
        
        
        let ref = Database.database().reference().child("User").child(message.chatPartnerId!)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in

            
            let user = User()
            
            if let dictonary = snapshot.value as? [String: AnyObject]{

                user.id = snapshot.key
                user.Email = dictonary["Email"] as? String
                user.Pass = dictonary["Pass"] as? String

            }

            let chatvc = self.storyboard?.instantiateViewController(withIdentifier: "ChatLogController") as! ChatLogController
            chatvc.toUserId = user.id!
            chatvc.email = user.Email!
            
            self.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(chatvc, animated: true)
            self.hidesBottomBarWhenPushed = false


        }, withCancel: nil)

    }
    
}

    

