//
//  SelectGroupContactsViewController.swift
//  Raabta
//
//  Created by rashid  mahmood on 8/20/19.
//  Copyright © 2019 Apple. All rights reserved.


import UIKit
import Firebase
import FirebaseDatabase


class SelectGroupContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var groupTableView: UITableView!
    @IBOutlet weak var groupNameTextField: UITextField!
    
        var groupEmails = [GroupChat]()
        var selectedEmails = [GroupChat]()
       // var selectArr = [GroupChat]()
        var databaseHandle : DatabaseHandle?
        var ref : DatabaseReference?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        groupNameTextField.backgroundColor = UIColor.clear
        groupNameTextField.borderStyle = UITextField.BorderStyle.none
        
        groupTableView.backgroundColor = UIColor.clear
        groupTableView .delegate  = self
        groupTableView.dataSource = self
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationItem.title = "New Group"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.purple]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "GroupBackground")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        self.groupTableView.isEditing = true
        self.groupTableView.allowsMultipleSelectionDuringEditing = true
        
        fetchUser()
        
//        self.tabBarController?.tabBar.isHidden = false
    }
    
      func fetchUser(){
        let ref = Database.database()
        databaseHandle = ref.reference().child("User").observe(.childAdded, with: { (snapshot) in
            
//             print(snapshot)
            
            if let dictionary = snapshot.value as? [String : AnyObject] {
               let groupEmails = GroupChat()
                    groupEmails.id = snapshot.key
                    groupEmails.email = dictionary["Email"] as? String
                
                    self.groupEmails.append(groupEmails)
                    self.groupTableView.reloadData()
            }
     
        }, withCancel: nil)
        
    }
    
    
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return groupEmails.count
    }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let email = groupEmails[indexPath.row]
        let cell = groupTableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
        cell.textLabel?.text = email.email
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.contentView.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    
          func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //   self.selectDeselectCell(tableView: groupTableView, indexPath: indexPath)
            print("deselect")
            
            }
//
//          func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
//            let cell = groupTableView.cellForRow(at: indexPath as IndexPath)
//
////            let text = cell?.textLabel?.text
////            if let text = text {
////                NSLog("did select and the text is \(text)")
////                print(text)
//            }
     //   }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // var selectArr = [String]()
        print(groupEmails[indexPath.row].email as Any)
    }
    
    @IBAction func createGroupButton(_ sender: Any) {
        
       // ref?.child("Groups").childByAutoId().setValue("sdsd")
        performSegue(withIdentifier: "groupChatLogSegue", sender: self)
        
//        var ref : DatabaseReference?
//               ref = Database.database().reference()
//
//               let newRef = ref?.child("Events")
//               //newRef?.child(eventName.text!).setValue(["name": eventName.text!, "city": eventCity.text!])
//
//                newRef?.child(eventName.text!).setValue(["name" : eventName.text!, "date" : eventDate.text!,
//                "city" : eventCity.text!, "zipcode" : eventZipcode.text!,
//                "street" : eventStreet.text!, "Nr" : eventNr.text!,
//                "location" : eventLocation.text!,
//                "organizer" : eventOrganizerName.text!,
//                "artist_dj_brand" : eventArtistDJ.text!,
//                "website" : eventWebsite.text!, "phone" : eventPhoneNo.text!,
//                "entry_price" : eventEntryPrice.text!,
//                "category_list" : eventCategoryList.text!
//                   ])

//        let chatvc = self.storyboard?.instantiateViewController(withIdentifier: "ChatLogController") as! ChatLogController
//        chatvc.email = user[indexPath.row].Email!
//        chatvc.toUserId = user[indexPath.row].toId!
//
//        hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(chatvc, animated: true)
    }
    
    
       //  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
//            var selectedContacts = selectedEmails[indexPath.row]
//                print(selectedContacts)
           // let cell = groupTableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
//
//            let cell = tableView.cellForRow(at: indexPath) as! ContactsCell
//            print(cell.textLabel?.text)
        //    self.selectDeselectCell(tableView: groupTableView, indexPath: indexPath)
          //      print("select")
            
      //      let selectedItem = indexPath
          //  print (selectedItem.row)
            
//
//             if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
//
//                tableView.cellForRow(at: indexPath)?.accessoryType = .none
//
//                   }
//             else {
//                    tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
////                if cell.isSelected == true{
////                    //selectedContacts.append(indexPath)
////                    print("cell is seleted")
////                    }
//
//                }
//
//            if cell.isSelected == true
//            {
//              //  selectedEmails.email == selectedEmails
//                print(selectedEmails)
//            }
//
            
//                let email = selectedContacts[indexPath]
//                    print(email)
                  
//        let cell = groupTableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
//        if cell.accessoryType == .checkmark {
//            cell.accessoryType = .none
//        }
//        else {
//            cell.accessoryType = .checkmark
        
       // accessoryButtonTappedForRowWith()
//        print(self.tableArray[indexPath.row])
//        tableView.deselectRow(at: indexPath, animated: true)
  //  }
    
   //
  

//          func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
//                         let cell = groupTableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath)
//
//            }
    

}

extension SelectGroupContactsViewController {
       // select and Deselect TableViewCell
       
    func  selectDeselectCell(tableView: UITableView, indexPath: IndexPath ) {
        //self.selectArr.removeAll()
       // let email = GroupChat()
        //print(email.email!)

        
        //fetchUser()
       // print(email.email!)
        
           //print(groupEmails)
//            let arr = GroupChat()
//        if arr.email = groupTableView?.indexPathForSelectedRow  {
//            print(arr)
//            for index in arr.email {
//                selectArr.append(groupEmails[indexPath.row])
//            }
//        }
//
//        print(selectArr)
        
        
    }
   }
