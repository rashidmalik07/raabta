//
//  NewMessageTableViewController.swift
//  Raabta
//
//  Created by rashid  mahmood on 7/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import CodableFirebase

class NewMessageTableViewController: UITableViewController {

    
    let cellID = "cellID"
    var user = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchUser()

    }
    
    func fetchUser(){
        
        Database.database().reference().child("User").observe(.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject]{
                let user = User()
                user.toId = snapshot.key

                user.Email = dictionary["Email"] as? String
                user.Pass = dictionary["Pass"] as? String
          //      user.toId = dictionary["toId"] as? String
                
                self.user.append(user)
                self.tableView.reloadData()
 
            }
            
        }, withCancel: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return user.count
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        let userr = user[indexPath.row]
        
        cell.textLabel?.text = userr.Email
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatvc = self.storyboard?.instantiateViewController(withIdentifier: "ChatLogController") as! ChatLogController
        chatvc.email = user[indexPath.row].Email!
        chatvc.toUserId = user[indexPath.row].toId!
        
        hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(chatvc, animated: true)
        
        
    }
    
}
