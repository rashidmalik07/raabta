//
//  GroupChat.swift
//  Raabta
//
//  Created by rashid  mahmood on 9/18/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class GroupChat: NSObject, Codable {
    
    var id    : String?
    var email : String?
    var selectedEmailForGroup : String?
    var indexPathForSelectedRow : String?
}
