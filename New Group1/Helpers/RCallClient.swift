//
//  RCallClient.swift
//  Raabta
//
//  Created by rashid  mahmood on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import Sinch

class RCallClient {

    static let sharedCallClient : SINClient = Sinch.client(withApplicationKey: "ac223997-29b5-47f3-b989-4efaf1c12c20", environmentHost: "clientapi.sinch.com", userId:UUID().uuidString)
    
    static func performSettings(){
        sharedCallClient.setSupportCalling(true)
        sharedCallClient.setSupportMessaging(true)
        sharedCallClient.setSupportPushNotifications(true)
    }
    
    static func startSharedClient(){
        sharedCallClient.start()
        sharedCallClient.startListeningOnActiveConnection()
    }
    
    static func stopSharedClient(){
        sharedCallClient.stopListeningOnActiveConnection()
        sharedCallClient.stop()
        sharedCallClient.terminate()
    }

    
    
}


