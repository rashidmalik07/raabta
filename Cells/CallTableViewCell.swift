//
//  CallTableViewCell.swift
//  Raabta
//
//  Created by rashid  mahmood on 11/15/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit


protocol CallTableViewCellProtocol: AnyObject{
    
    //func callingEmail (_sender: CallTableViewCell, callingId : String)
    func callSegueFromCell(myData dataobject: String)
    
}


class CallTableViewCell: UITableViewCell {
    
    var chatOBject : GroupChat!
    
    @IBOutlet var voiceCallButton : UIButton!
    @IBOutlet var videoCallButton : UIButton!

    weak var delegate : CallTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func voiceCallButtonPressed(_sender:AnyObject){
        
       // delegate?.callingEmail(_sender: self, callingId : chatOBject.email!)
        let mydata = chatOBject.email!
        if(self.delegate != nil){ //Just to be safe.
            self.delegate?.callSegueFromCell(myData: mydata)
        }

       // print("\(String(describing: chatOBject.email)) ")
        
    }
    
    @IBAction func videoCallButtonPressed(_sender:AnyObject){
        print("\(String(describing: chatOBject.email)) ")
    }

}
